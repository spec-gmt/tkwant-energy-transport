.. _quantum_dyn:

Quantum dynamic simulations
===========================



Let us consider a finite system *S* without interactions. We write the
Hamiltonian as

.. math::


       \hat{H}(t) =  \sum_{i,j \in S} H_{ij}(t) \, \hat{c}^\dagger_i \hat{c}_j

where :math:`\hat{c}^\dagger_i` (:math:`\hat{c}_j`) is a creation
(destruction) operator for a one-particle state on site :math:`i`
(:math:`j`), and :math:`H_{ij}(t)` are matrix elements of the
Hamiltonian. We suppose that some time-dependent perturbation starts at
:math:`t > 0`. At initial time :math:`t = 0`, and eigenstate
:math:`| \alpha \rangle` obey the stationary Schrödinger equation

.. math::


       \hat{H}(t=0) | \alpha \rangle =  E_\alpha | \alpha \rangle .

The states can be normalized to form a normalized basis

.. math::


       1 = \sum_{\alpha} | \alpha \rangle \langle \alpha | , \qquad \delta_{\alpha , \alpha'} = \langle \alpha | \alpha' \rangle .

To continue, let us consider an arbitrary time-dependent state
:math:`| \psi (t) \rangle`, which can be expressed by a linear
superposition of the eigenstates :math:`| \alpha \rangle` as

.. math::


       | \psi (t) \rangle = \sum_{\alpha} c_{\alpha}(t)  | \alpha \rangle .

Note that the time dependence is governed by the time-dependent
coefficients :math:`c_{\alpha}(t) = \langle \alpha | \psi (t) \rangle`.
The dynamics of the state :math:`| \psi (t) \rangle` is described by the
time-dependent Schrödinger equation

.. math::


       i \partial_t  | \psi (t) \rangle =  \hat{H}(t) | \psi (t) \rangle

Often we are interested in the (time dependent) expectation value of
some observable, that is represented by the local operator
:math:`\hat{A}` with matrix elements :math:`\hat{A}_{ij}` on site
:math:`i` (:math:`j`). The expectation value of state :math:`\alpha` is

.. math::


       \langle A_{i, j, \alpha}(t) \rangle = \langle \psi_{\alpha}(i, t) | A_{i,j} | \psi_{\alpha}(j, t) \rangle

In order to calculate physical observales like average particle numbers
or currents, we have to evolve all filled one-body states and calculate
the thermal average

.. math::


       \langle A_{i,j}(t) \rangle =  \sum_\alpha \int_{-\infty}^{\infty} \frac{d E}{2 \pi} f_\alpha (E) \langle \psi_{\alpha E}(i, t)  | A_{i,j} | \psi_{\alpha E}(j, t) \rangle

We refer to Refs. `[1] <#references>`__ and `[2] <#references>`__ for a
more elaborate discussion of time-resolved quantum simulations.

Classification
--------------

The time-dependent scattering problem can be classified by several
different cases.

**Closed system** refers to a finite scattering region *S* with hard
boundaries. The Hamiltonian for a closed system without interactions has
a typical form

.. math::


       \hat{H}(t) =  \sum_{i,j \in S} H_{ij}(t) \, \hat{c}^\dagger_i \hat{c}_j .

All propagating waves in the scattering region get totally reflected at
the boundary.

**Open system** refers to a finite scattering region *S* connected to
semi-infinite periodic leads *L*. The Hamiltonian for an open system
without interactions has the typical form

.. math::


       \hat{H}(t) =  \sum_{i,j \in S} H_{ij}(t) \, \hat{c}^\dagger_i \hat{c}_j + \sum_{i,j \in L} H_{ij} \, \hat{c}^\dagger_i \hat{c}_j + \sum_{i \in S, j \in L} H_{ij}(t) \, \hat{c}^\dagger_i \hat{c}_j + h.c. .

In contrast to the previous case, a propagating wave is not reflected at
the system-lead boundary. However, a wave propagating into a semi
infinite lead will not come back into the scattering system any more.
Open quantum systems require special boundary conditions to account for
this behavior. Such boundary conditions were explored in Ref.
`[3] <#references>`__ and are subject of a separate tutorial.

Note that even if the leads contain some uniform time-dependent voltage,
the Hamiltonian can always be brougth into the form above with a
time-independent lead by an appropriate gauge transformation.

|image0|

Figure: Sketch of a scattering problem with an open quantum system. It
consists of a central scattering region :math:`\bar{0}` with
semi-infinite leads (:math:`\bar{1}`, :math:`\bar{2}` and
:math:`\bar{3}`) attached. Some sites and couplings in the scattering
region are time dependent (time-dependent on-site elements in green,
time-dependent coupling elements in blue). Figure taken out of Ref. [3].

**initial state**

-  equilibrium initial condition, the so-called scattering state

-  an arbitrary nonequilibrium initial condition

The onebody states ``onebody.ScatteringStates`` discussed in this
chapter is aimed for open quantum systems starting in an equilibium initial
condition.
To solve general time-dependent one-body Schrödinger equations with an arbitray
initial condition and also closed systems, one has to use the
onebody state  ``onebody.WaveFunction``, discussed in :ref:`onebody_advanced`.

``manybody.State`` is the manybody analogue of ``onebody.ScatteringStates``.
``manybody.State`` is intended for open manybody systems starting in an initial
scattering state.

.. |image0| image:: system.png

Onebody dynamics
----------------

We consider the stationary Schrödinger equation

.. math::


       H(t=0) | \psi_{\alpha, E}^{\text{stat}} \rangle = E | \psi_{\alpha, E}^{\text{stat}} \rangle

For the finite scattering region that is connected to semi-infinite leads, the states
:math:`| \psi_{\alpha, E}^{\text{stat}} \rangle` are the so-called scattering
states. They are labeled by an index :math:`\alpha` and an energy :math:`E`.
We like to take the scattering state as initial state and solve the
time-dependent Schrödinger equation in time :math:`t`, starting at some initial
time :math:`t = 0`:

.. math::


       i \partial_t | \psi_{\alpha, E} (t) \rangle = H(t) | \psi_{\alpha, E} (t) \rangle , \qquad
       | \psi_{\alpha, E} (t=0) \rangle = | \psi_{\alpha, E}^{\text{stat}} \rangle


The tkwant code to perform above task is


.. jupyter-execute::
    :hide-code:

    from cmath import exp
    from math import cos, sqrt, pi
    import numpy as np
    import matplotlib
    from matplotlib import pyplot as plt
    import kwant
    import tkwant
    from tkwant import onebody, manybody

    def create_system(L):

        def onsite_potential(site, time):
            return 1

        # system building
        lat = kwant.lattice.square(a=1, norbs=1)
        syst = kwant.Builder()

        # central scattering region
        syst[(lat(x, 0) for x in range(L))] = 1
        syst[lat.neighbors()] = -1
        # time dependent onsite-potential V(t) at leftmost site
        syst[lat(0, 0)] = onsite_potential

        # add leads
        sym = kwant.TranslationalSymmetry((-1, 0))
        lead_left = kwant.Builder(sym)
        lead_left[lat(0, 0)] = 1
        lead_left[lat.neighbors()] = -1
        syst.attach_lead(lead_left)

        return syst

    syst = create_system(2).finalized()


.. jupyter-execute::

    psi = onebody.ScatteringStates(syst, energy=1, lead=0, tmax=500)[0]

The class ``onebody.ScatteringStates()`` returns a sequence of open scattering states.
Here, we selected the mode with index zero,
and the resulting onebody wavefunction instances is called ``psi`` in this example.
The wavefunction ``psi`` has an ``evolve()`` and an ``evaluate()``
method, to evolve it forward in time and to evaluate an operator. We will
show their usage in the following.
Note that we did not show the construction of the system for clarity,
but which is similar to the one in
:ref:`getting_started`.




Time evolution
~~~~~~~~~~~~~~

The onebody wavefunction has an ``evolve()`` method to propagate it foreward in time.
The following code will evolve the state ``psi`` up to time :math:`t = 10`:

.. jupyter-execute::

    psi.evolve(time=10)

Note that the evolution time :math:`t` must be less than the maximal
time ``tmax`` passed to ``onebody.ScatteringStates()`` during instantiation.

Observables
~~~~~~~~~~~

Several observables that are represented by
local operators are already defined in ``kwant.operators``. The
average electron density on site :math:`i`

.. math::


       \langle \hat{c}^\dagger_i \hat{c}_i (\alpha, t) \rangle = \langle \psi_{\alpha}(i, t) | \hat{c}^\dagger_i \hat{c}_i | \psi_{\alpha}(i, t) \rangle

for instance, is available via the corresponding density operator

.. jupyter-execute::

    density_operator = kwant.operator.Density(syst)

The wavefunction ``psi`` provides an ``evaluate()`` method, that can
be called directly with an operator instance

.. jupyter-execute::

    density = psi.evaluate(density_operator)

The expectatin value is taken at the current time of the state ``psi``.
We refer to the documentation in ``kwant.operators`` for additional observables.


Manybody dynamics
-----------------

In the manybody case, the expectation value of an observable :math:`A` is obtainded by
calculating the single-particle expectation values by using the
wavefunction and weighing it with the Fermi statistics

.. math::


       \langle A_{i,j}(t) \rangle =  \sum_\alpha \int_{-\infty}^{\infty} \frac{d E}{2 \pi} f_\alpha (E) \langle \psi_{\alpha E}(i, t)  | A_{i,j} | \psi_{\alpha E}(j, t) \rangle

In the previous equation,
:math:`| \psi_{\alpha, E}\rangle` are the scattering states of the system,
that are solutions of the time-dependent onebody Schrödinger equation
we have defined before.
:math:`\alpha` is a super index, including mode, lead (and
probably other) degrees of freedom,
:math:`f_\alpha (E)` is the Fermi function of the electrode associated
to mode :math:`\alpha` and :math:`A_{ij}` is the matrix element of the
observable :math:`A` corresponding to site :math:`i` and :math:`j`.

States
~~~~~~

``tkant.manybody.States`` can be used to evolve the filled
one-body states and calculate the thermal averge defined above.
By default, ``tkant.manybody.States`` assumes an identical occupation
for each lead with chemical potential :math:`\mu = 0`, temperature :math:`T = 0`
and the noninteracting Fermi dirac distribution function, which, in that case
becomes :math:`f_\alpha (E) = \Theta(-E)`.
The expectation value of an observable :math:`\hat{A}` becomes

.. math::


       \langle A_{i,j}(t) \rangle =  \sum_\alpha \int_{-\infty}^{0} \frac{d E}{2 \pi} \langle \psi_{\alpha E}(i, t)  | A_{i,j} | \psi_{\alpha E}(j, t) \rangle


The manybody state is initialized as:

.. jupyter-execute::

    state = manybody.State(syst, tmax=10)

The ``state`` instance has an ``evolve()`` and an ``evaluate()``
method, to evolve it forward in time and to evaluate an operator. We will
show their usage in the following.


Time evolution
~~~~~~~~~~~~~~

A manybody wavefunction ``state`` can be evolved forward to time :math:`t` by calling the
method ``evolve()`` with the time argument:

.. jupyter-execute::

    state.evolve(time=5)

Note that the evolution time :math:`t` must be less than the maximal
time ``tmax`` passed to ``manybody.State()`` during instantiation.

Observables
~~~~~~~~~~~

To calculate an observable at time :math:`t`, the manybody state
provides the method ``evaluate()``. It must be called with the
corresponding operator:

.. jupyter-execute::

    density = state.evaluate(density_operator)

All operators defined in ``kwant.operators`` can be used.

Numerical accuracy
~~~~~~~~~~~~~~~~~~

The integral over the energy *E* in the expression for the manybody expectation value is
done with the help of a numerical quadrature.
The numerical error from this approximation determines to a large extend the numerical accuarcy of the result.
``manybody.State()`` provides the two methods ``estimate_error()``
and ``refine_intervals()`` to check and to refine the numerical integral in a semi-automatic way. The
method ``estimate_error()`` estimates the numerical error integration quadrature:

.. jupyter-execute::

    error = state.estimate_error()
    print('estimated integration error= {}'.format(error))

The method ``refine_intervals()`` can be used to decrease the quadrature error:

.. jupyter-execute::

    state.refine_intervals();

``refine_intervals()`` estimates an error of each integration interval
and performs local refinement, if the error is too large. The threshold value
has a default value, but can by changed in order to increase or decrase the accuracy.

Note that the refinement step can be very expensive, as all onebody
states that contribute to a new interval must be evolved from the
initial time up to the current state time. It is therefore recommended
to refine the intervals only at early simulation times or
after an initial perturbation. Alternatively, one could also try
refine only once at the beginning, but with a higher precision.
To have resonable computational performance, the intervals should remain
constant at later simulation times.



Summary
-------

Both states, ``onebody.ScatteringStates()`` and
``manybody.State()``, are quite similiar to use. The first step
in both cases is to define an observable:

.. jupyter-execute::

   density_operator = kwant.operator.Density(syst)

The two pieces of pseudo code compare the standard setup for both states:

**Onebody**

.. jupyter-execute::

   psi = onebody.ScatteringStates(syst, energy=1, lead=0, tmax=10)[0]
   psi.evolve(time=5)
   density = psi.evaluate(density_operator)

**Manybody**

.. jupyter-execute::

   state = manybody.State(syst, tmax=10)
   state.evolve(time=5)
   density = state.evaluate(density_operator)

**Customize settings**

To change the default behavior, see the tutorial section:

:ref:`onebody_advanced`

:ref:`manybody_advanced`

References
----------

[1] J. Weston and X. Waintal, `Towards realistic time-resolved
simulations of quantum
devices <https://link.springer.com/article/10.1007%2Fs10825-016-0855-9>`__, J. Comput.
Electron. **15**, 1148 (2016).
`[arXiv] <https://arxiv.org/abs/1604.01198>`__

[2] B. Gaury, J. Weston, M. Santin, M. Houzet, C. Groth and X. Waintal,
`Numerical simulations of time-resolved quantum electronics
<https://www.sciencedirect.com/science/article/pii/S0370157313003451?via%3Dihub>`__,
Phys. Rep. **534**, 1 (2014).
`[arXiv] <https://arxiv.org/abs/1307.6419>`__

[3] J. Weston and X. Waintal, `Linear-scaling source-sink algorithm for
simulating time-resolved quantum transport and
superconductivity <https://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.134506>`__,
Phys. Rev. B **93**, 134506 (2016).
`[arXiv] <https://arxiv.org/abs/1510.05967>`__
