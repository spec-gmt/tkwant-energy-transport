.. _onebody_advanced:

Advanced onebody settings
=========================

.. jupyter-execute::
    :hide-code:

    from cmath import exp
    from math import cos, sqrt, pi
    import numpy as np
    import matplotlib
    from matplotlib import pyplot as plt
    import kwant
    import tkwant
    from tkwant import onebody

    def create_system(L):

        def onsite_potential(site, time):
            return 1

        # system building
        lat = kwant.lattice.square(a=1, norbs=1)
        syst = kwant.Builder()

        # central scattering region
        syst[(lat(x, 0) for x in range(L))] = 1
        syst[lat.neighbors()] = -1
        # time dependent onsite-potential V(t) at leftmost site
        syst[lat(0, 0)] = onsite_potential

        # add leads
        sym = kwant.TranslationalSymmetry((-1, 0))
        lead_left = kwant.Builder(sym)
        lead_left[lat(0, 0)] = 1
        lead_left[lat.neighbors()] = -1
        syst.attach_lead(lead_left)
        syst.attach_lead(lead_left.reversed())

        return syst

    syst = create_system(5).finalized()



The time-dependent one-particle Schrödinger equation has the generic form

.. math::


       i \partial_t | \psi_{\alpha, E} (t) \rangle = H(t) | \psi_{\alpha, E} (t) \rangle

and has to be solved together with an initial condition
:math:`| \psi_{\alpha, E} (0) \rangle` for time :math:`t = 0`.

In part :ref:`quantum_dyn`, the onebody solver ``onebody.Modes``
has been introduced. This solver simplifies the solution of the time-dependent onebody Schrödinger equation for
scattering wave functions.
tkwant provides in addition the onebody solver ``onebody.WaveFunction()``.
It is the low-level version to solve more general the time-dependent onebody Schrödinger equation.
The function ``onebody.WaveFunction()`` also returns an onebody wavefunction with
an ``evolve()`` and an ``evaluate()`` method, but the function is more flexible.
``onebody.WaveFunction()`` handles arbitrary states (eigenstate or non-eigenstates) for open or closed
onebody quantum systems. Its usage is shown in the following.

Eigenstates
~~~~~~~~~~~

For an open quantum system with semi-infinite leads,
the Schrödinger equation is defined in the infinite
domain, which makes a direct time-dependent simulation not possible.
tkwant uses a trick for the case that the initial state is an eigenstate of the
system.

In the following, we consider the case that the initial state is an
eigenstate of the stationary Schrödinger equation

.. math::


       H(t=0) | \psi_{\alpha, E}^{\text{stat}} \rangle = E | \psi_{\alpha, E}^{\text{stat}} \rangle

For a finite scattering region connected to semi-infinite leads,
:math:`| \psi_{\alpha, E}^{\text{stat}} \rangle` are the scattering
states labeled by :math:`\alpha`. The time-dependent one-body Schrödinger equation is

.. math::


       i \partial_t | \psi_{\alpha, E} (t) \rangle = H(t) | \psi_{\alpha, E} (t) \rangle , \qquad
       | \psi_{\alpha, E} (t=0) \rangle = | \psi_{\alpha, E}^{\text{stat}} \rangle .

We can get rid of the trivial dynamics in the infinite domain by
introducing

.. math::


        | \psi_{\alpha, E}(t) \rangle  = ( | \tilde{\psi}_{\alpha, E} (t) \rangle  + | \psi_{\alpha, E}^{\text{stat}}  \rangle ) e^{-iEt}, \qquad \text{with} \,\, | \tilde{\psi}_{\alpha, E}(0) \rangle = 0

The time-dependent Schrödinger equation becomes

.. math::


       i \partial_t | \tilde{\psi}_{\alpha, E} (t) \rangle = [H(t) - E] | \tilde{\psi}_{\alpha, E} (t) \rangle
       + H(t)| \psi_{\alpha, E}^{\text{stat}} \rangle + \Sigma | \tilde{\psi}_{\alpha, E} (t) \rangle

where :math:`H(t)| \psi_{\alpha, E}^{\text{stat}} \rangle` is called the
source and :math:`\Sigma` the sink term, a diagonal matrix that is zero
in the central region and takes complex values in a finite portion of
the lead. We refer to Ref. `[1] <#references>`__ and
`[3] <#references>`__ for details.

The advantage of this formulation is that the initial condition
:math:`| \tilde{\psi}_{\alpha, E}(0) \rangle` vanishes everywhere.
During the time evolution one has to simulate only local fluctuation of
this quantity in a finite region centered around the initial scattering
region.

The code of the examples below is in fact very similar to the formulas.
In the line

.. jupyter-execute::

    scattering_states = kwant.wave_function(syst, energy=1, params={'time':0})
    lead, mode = 0, 0
    psi_st = scattering_states(lead)[mode]

we construct all scattering states corresponding to energy :math:`E=1`
by solving the stationary Schrödinger equation. This task is easily done
by ``kwant``. Note that the time argument :math:`t` of the Hamiltonian
value functions must be set to the initial time
:math:`t=0` via `params`. A scattering state is selected by its lead and mode number.
We finally initialize the one-body wavefunction by calling ``onebody.WaveFunction`` and
pass the scattering state ``psi_st`` as initial state via keyword
``psi_init``:

.. jupyter-execute::

    boundaries = [tkwant.leads.SimpleBoundary(tmax=500)] * len(syst.leads)
    psi = onebody.WaveFunction.from_kwant(syst, psi_st, boundaries=boundaries, energy=1)

Additional boundary conditions must be provided for an open system.
The Hamiltonian of the system is bound to ``syst``.
The keyword ``energy`` tells ``tkant.solve`` that ``psi_init`` is an
eigenstate of the stationary Schrödinger equation corresponding to
energy :math:`E`. The solver ``tkant.solve`` then performs the splitting
into :math:`| \tilde{\psi} (t) \rangle` and integrate the corresponding
Schrödinger equation in time. Note that even though the Schrödinger equation is numerically
solved for :math:`| \tilde{\psi} (t) \rangle`, the state ``psi``
corresponds to the full state :math:`| \psi (t) \rangle`. The code in the above
two blocks is identical to

.. jupyter-execute::

    psi = onebody.ScatteringStates(syst, energy=1, lead=0, tmax=500)[mode]


Non-eigenstates
~~~~~~~~~~~~~~~

Arbitrary initial states, which are not an eigenstate of the system can
also be studied. Note that in this case we have to integrate directly
the Schrödinger equation

.. math::


       i \partial_t | \psi_{\alpha} (t) \rangle = H(t) | \psi_{\alpha} (t) \rangle

in time, without the splitting in
:math:`| \tilde{\psi}_{\alpha, E} (t) \rangle` as before. To initialize
the solver ``tkant.solve`` with an initial state that is not an
eigenstate of the system, only the keyword ``energy`` has to be omitted.


Wavefunction and observables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The wavefunction instance ``psi`` obtained by the onebody solvers
can be evolved in time with the method ``evolve``.
The state ``psi`` stores internally the time. It cannot evolve backward
in time.
The wavefunction :math:`| \psi (t) \rangle` can be
accessed with the attribute ``psi``. One can use this mechanism to evaluate an operator.

.. jupyter-execute::

    psi = onebody.WaveFunction.from_kwant(syst, psi_st, boundaries=boundaries, energy=1.)
    density_operator = kwant.operator.Density(syst)
    psi.evolve(time=10)
    density = density_operator(psi.psi())

The last line in above code is similar to:

.. jupyter-execute::

    density = psi.evaluate(density_operator)


Boundary conditions
~~~~~~~~~~~~~~~~~~~

Special boundary conditions have to be provided in
order to solve the dynamic equations for an open quantum systems (with leads). The
``tkwant.solve`` class can internally account for the boundary
conditions, but a maximal simulation time must be provided by by
the keyword ``tmax``. Aternatively, we can also provide ``tkwant.solve``
the boundary conditions by the keyword ``boundary``.

.. jupyter-execute::

    boundaries = [tkwant.leads.SimpleBoundary(tmax=500)] * len(syst.leads)
    psi = onebody.WaveFunction.from_kwant(syst, psi_st, boundaries=boundaries, energy=1.)

For closed quantum systems (without leads), no boundary conditions are needed.

Time integration
~~~~~~~~~~~~~~~~

The time integration can be changed by prebinding values with the module
``functool.partial`` to the onebody solver. In the current example, we
change the relative tolerance ``rtol`` of the time-stepping algorithm:

.. jupyter-execute::

    import functools as ft
    onebody_solver = ft.partial(tkwant.onebody.solvers.default, rtol=1E-5)
    psi = onebody.WaveFunction.from_kwant(syst=syst, boundaries=boundaries, psi_init=psi_st, energy=1., solver_type=onebody_solver)

Saving and restarting states
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes we might like to save a state in order to resume a calculation
on a later stage. An easy way is to to use the external library ``dill``:

.. jupyter-execute::

    import dill
    saved = dill.dumps(psi)

The saved object ``saved`` can be stored.
Recovering the state later on in order to continue the
calculation is possible by using

.. jupyter-execute::

    new_psi = dill.loads(saved)

See :ref:`restarting` for the complete code.
Saving a state with ``dill`` might not work for certain non-python kernels like
``onebody.kernel.Simple`` and ``onebody.kernel.SparseBlas``.

Examples
~~~~~~~~

:ref:`closed_system`

:ref:`open_system`

:ref:`restarting`

References
~~~~~~~~~~

[1] J. Weston and X. Waintal, `Towards realistic time-resolved
simulations of quantum
devices <https://link.springer.com/article/10.1007%2Fs10825-016-0855-9>`__, J. Comput.
Electron. **15**, 1148 (2016).
`[arXiv] <https://arxiv.org/abs/1604.01198>`__

[2] B. Gaury, J. Weston, M. Santin, M. Houzet, C. Groth and X. Waintal,
`Numerical simulations of time-resolved quantum electronics
<https://www.sciencedirect.com/science/article/pii/S0370157313003451?via%3Dihub>`__,
Phys. Rep. **534**, 1 (2014).
`[arXiv] <https://arxiv.org/abs/1307.6419>`__

[3] J. Weston and X. Waintal, `Linear-scaling source-sink algorithm for
simulating time-resolved quantum transport and
superconductivity <https://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.134506>`__,
Phys. Rev. B **93**, 134506 (2016).
`[arXiv] <https://arxiv.org/abs/1510.05967>`__


