from math import sin, pi
import numpy as np
from matplotlib import pyplot as plt
import kwant
import tkwant
import dill


def make_system(a=1, t=1.0, radius=10):
    """Make a tight binding system on a single square lattice"""
    # `a` is the lattice constant and `t` the hopping integral
    # both set by default to 1 for simplicity.

    lat = kwant.lattice.square(a, norbs=1)

    syst = kwant.Builder()

    # Define the quantum dot
    def circle(pos):
        (x, y) = pos
        return x ** 2 + y ** 2 < radius ** 2

    syst[lat.shape(circle, (0, 0))] = 4 * t
    syst[lat.neighbors()] = -t

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[(lat(0, j) for j in range(-radius//2 + 1, radius//2))] = 4 * t
    lead[lat.neighbors()] = -t

    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    return syst


# add a time-dependent voltage on top of the leads
def faraday_flux(time):
    return 0.1 * (time - 10 * sin(0.1 * time)) / 2


def main():
    syst = make_system()

    # add a time-dependent voltage to lead 0 -- this is implemented
    # by adding sites to the system at the interface with the lead and
    # multiplying the hoppings to these sites by exp(-1j * faraday_flux(time))
    extra_sites = tkwant.leads.add_voltage(syst, 0, faraday_flux)
    lead_syst_hoppings = [(s, site) for site in extra_sites
                          for s in syst.neighbors(site)
                          if s not in extra_sites]

    syst = syst.finalized()

    # create an observable for calculating the current flowing from the left lead
    current_operator = kwant.operator.Current(syst, where=lead_syst_hoppings, sum=True)

    tmax = 100 * pi

    # create a time-dependent wavefunction that starts in a scattering state
    # originating from the left lead
    scattering_states = kwant.wave_function(syst, energy=1., params={'time': 0})
    psi_st = scattering_states(0)[0]
    boundaries = [tkwant.leads.SimpleBoundary(tmax=tmax)] * len(syst.leads)
    psi = tkwant.onebody.WaveFunction.from_kwant(syst=syst, boundaries=boundaries,
                                                 psi_init=psi_st, energy=1.)

    # evolve forward in time, calculating the current
    times = np.arange(0, tmax / 2)
    current = []
    for time in times:
        psi.evolve(time)
        current.append(psi.evaluate(current_operator))

    # now we want to "save" our progress, and start again with another solver
    # note that we still have to provide the same boundary condition (via 'tmax')
    # as that originally used to produce 'saved'.
    saved = dill.dumps(psi)
    new_psi = dill.loads(saved)

    times2 = np.arange(tmax / 2, tmax)

    for time in times2:
        new_psi.evolve(time)
        current.append(new_psi.evaluate(current_operator))

    plt.plot(np.append(times, times2), current)
    plt.xlabel(r'time $t$')
    plt.ylabel(r'current $I$')
    plt.show()


if __name__ == '__main__':
    main()
