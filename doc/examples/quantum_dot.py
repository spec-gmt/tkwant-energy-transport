# -*- coding: utf-8 -*-
# Copyright 2016-2019 tkwant authors.
#
# This file is part of tkwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of tkwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.



#################################
# Quantum dot with time-dependent potential
# Model : infinite 1D chain at a constant potential, except one site where the potential will perform jump (heaviside)

import numpy as np
import matplotlib.pyplot as plt
import time as timer
from math import sqrt, pi

# To be imported first before kwant and tkwant (Avoid Fedora problems)
from mpi4py import MPI 

import kwant
import tkwant

# Comment the next line if you don't have a latex distribution installed
plt.rcParams['text.usetex'] = True

plt.rcParams['figure.figsize'] = (11.69,8.27)
plt.rcParams['axes.formatter.useoffset'] = True
plt.rcParams['axes.formatter.limits'] = (-2, 2)

plt.rcParams['axes.grid'] = True
plt.rcParams['font.size'] = 20

# can be installed through pip ('pip3 install csv_manager'), makes saving to and plotting from CSVs easier
# import csv_manager

# Define the region of study : add the time-dependent potential
def qdot_potential(site, time, t0, ε0, Δε):
    if time > t0:
        return ε0 + Δε
    else:
        return ε0

def make_system(a, γ, γc):
    # Create the Bravais lattice and the system
    lat = kwant.lattice.chain(a, norbs = 1)
    syst = kwant.Builder()  

    # lat(1) is the dot, the rest belongs already formally to the leads
    syst[(lat(0))] = 0
    syst[(lat(1))] = qdot_potential
    syst[(lat(2))] = 0
    syst[lat.neighbors()] = - γc  

    # Define and attach the leads
    lead = kwant.Builder(kwant.TranslationalSymmetry((-a,)))
    lead[lat(0)] = 0
    lead[lat.neighbors()] = - γ

    added_sites_left = syst.attach_lead(lead, add_cells=1)    
    # Append lat(0) to the list, to calculate the heat current between lat(0) and lat(1)
    added_sites_left.append(lat(0))

    added_sites_right = syst.attach_lead(lead.reversed(), add_cells=1)
    # Append lat(0) to the list, to calculate the heat current between lat(2) and lat(1)
    added_sites_right.append(lat(2))

    return syst, lat, added_sites_left, added_sites_right


def am_master():
    return MPI.COMM_WORLD.rank == 0


def main():
    # Create the initial quantum dot system
    a = 1.  # Lattice spacing

    γ = 1. / (a ** 2) # Hopping term
    γc = 0.2 * γ

    λ =  1. # Dimensionless coefficient, the closer to the wide band limit, but requires more calculation time
    γ = λ * γ  # rescaled lead hopping   
    γc = sqrt(λ) * γc  # dot-lead hopping
    
    Γ = 4 * γc*γc / γ # Energy scaling unit

    # Simulation times
    tmax = 6. / Γ  # Duration of the simulation
    dt = 0.005 / Γ  # Interval between two measures of the operator over the system
    times = np.arange(0, tmax, dt)

    t0 =  0.0 / Γ # Beginning of the pulse : pulse * heaviside(t - t0) 
    μL =  0.5 * Γ # Chemical potential in the left lead
    μR = -0.5 * Γ # Chemical potential in the right lead
    TL =  1.0 * Γ # Temperature in the left lead
    TR =  0.0 * Γ # Temperature in the right lead
    ε0 =  0.5 * Γ # initial dot energy level
    Δε =  2.5 * Γ # change of dot energy level   

    # Create system
    builder, lat, added_sites_left, added_sites_right = make_system(a, γ, γc)
    syst = builder.finalized()

    lead_heat_current_right_op = tkwant.operator.LeadHeatCurrent(syst, chemical_potential=μR, added_lead_sites=added_sites_right, baseOperator='kinetic+')
    lead_heat_current_left_op = tkwant.operator.LeadHeatCurrent(syst, chemical_potential=μL, added_lead_sites=added_sites_left, baseOperator='kinetic+')


    if am_master():
        used_hoppings_right = [(i, j) for (i,j) in lead_heat_current_right_op.hoppings]
        used_hoppings_left = [(i, j) for (i,j) in lead_heat_current_left_op.hoppings]

        print("Hoppings used by the heat current: ")
        print("Right=", used_hoppings_right)
        print("Left=", used_hoppings_left)

    
    energy_current_op = tkwant.operator.EnergyCurrent(syst, where=[(lat(0), lat(1)), (lat(2), lat(1))], baseOperator='kinetic+', sum=False)
    energy_source_op  = tkwant.operator.EnergySource(syst, where=[lat(1)], baseOperator='kinetic+', sum=True)
    energy_density_op  = tkwant.operator.EnergyDensity(syst, where=[lat(1)], baseOperator='kinetic+', sum=True)
    energy_current_divergence_op = tkwant.operator.EnergyCurrentDivergence(syst, where=[lat(1)])
    

    # Occupation, for each lead
    occupation = [None] * len(syst.leads)
    occupation[0] = tkwant.manybody.lead_occupation(chemical_potential=μL, temperature=TL)
    occupation[1] = tkwant.manybody.lead_occupation(chemical_potential=μR, temperature=TR)

    # Initialize the solver (to solve t-dep SEQ)
    solver = tkwant.manybody.State(syst, tmax, occupation, params={'t0': t0, 'ε0': ε0, 'Δε': Δε}, error_op=energy_density_op)

       
    energy_current = []
    energy_source = []    
    energy_density = []
    dt_energy_density = []
    heat_current_left = []
    heat_current_right = []
    energy_current_divergence = []

    # Have the system evolve forward in time, calculate the operator's expectation values at each time step 

    start = timer.perf_counter()
    for time in times:
        # evolve scattering states in time
        solver.evolve(time)
        solver.refine_intervals(atol=1E-6, rtol=1E-6)
     
        energy_current.append(solver.evaluate(energy_current_op))    
        energy_source.append(solver.evaluate(energy_source_op))
        energy_density.append(solver.evaluate(energy_density_op))
        heat_current_left.append(solver.evaluate(lead_heat_current_left_op))
        heat_current_right.append(solver.evaluate(lead_heat_current_right_op))
        energy_current_divergence.append(solver.evaluate(energy_current_divergence_op))

        if(time > t0):
            dt_energy_density.append((energy_density[-1] - energy_density[-2]) / dt)
        else:
            dt_energy_density.append(float('nan')) 
 

        if am_master():
            print("Elapsed time: {:.1f}s,  progress = {:.2f} %".format(timer.perf_counter() - start, time/tmax * 100))

  

    if am_master():
        # Rescale results
        times = np.array(times) * Γ
        energy_current = np.array(energy_current) / Γ**2
        energy_source = np.array(energy_source) / Γ**2
        heat_current_left = np.array(heat_current_left) / Γ**2
        heat_current_right = np.array(heat_current_right) / Γ**2
        
        energy_density = np.array(energy_density) / Γ
        dt_energy_density = np.array(dt_energy_density) / Γ**2
        energy_current_divergence = np.array(energy_current_divergence) / Γ**2

        # Plot the figure 
        plot = True
        if plot: 
            plt.plot(times, energy_current[:, 0], label = "Left outgoing current $I^E_{0,1}$")
            plt.plot(times, energy_current[:, 1], label = "Right outgoing current $I^E_{2,1}$")
            plt.plot(times, energy_source, label = "Source $S_1$")
            plt.plot(times, dt_energy_density, label = "Density time derivative $\\partial_t \\varepsilon_1$")
            plt.plot(times, heat_current_left, label = "Left lead outgoing heat flux")
            plt.plot(times, heat_current_right, label = "Right lead outgoing heat flux")
            plt.plot(times, energy_current_divergence + dt_energy_density,
                label = "$\\partial_t \\varepsilon_1 + I^E_{0,1} + I^E_{2,1}$", linestyle='None',
                    marker='.', markersize=6, markevery=4)   
            # The previous curve doesn't superimpose over the source one because the time step isn't small enough,
            # making the time step smaller makes the calculation last longer unfortunately.  
            plt.xlabel('time $[\\hbar / \\Gamma]$')
            plt.ylabel('Energy flux $[\\Gamma^2 / \\hbar]$')
            plt.legend()
            plt.show()    

        # For the following line to work, csv_manager needs to be installed through pip: pip3 install csv_manager and uncomment line 37
        # csv_manager.writer.write([['time'] + list(times), ['energy_current_left'] + list(energy_current[:, 0]), \
        #                           ['energy_current_right'] + list(energy_current[:, 1]), ['energy_source'] + list(energy_source), \
        #                           ['energy_density'] + list(energy_density), ['dt_energy_density'] + list(dt_energy_density), \
        #                           ['heat_current_left'] + list(heat_current_left), ['heat_current_right'] + list(heat_current_right)], \
        #                               'currents_and_densities.csv', 'columns')
  
if __name__ == '__main__':
    main()


