from math import sin, pi
import warnings
import time as timer
import matplotlib.pyplot as plt

with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    import kwant
    import tkwant


def am_master():
    """Return true for the MPI master rank"""
    return tkwant.mpi.get_communicator().rank == 0


def plot_currents(times, tkwant_current, kwant_current):
    plt.plot(times, tkwant_current, lw=3, label='tkwant')
    plt.plot([times[0], times[-1]], [kwant_current] * 2, lw=2, label='kwant')
    plt.legend(loc=4)
    plt.xlabel(r'time $t$')
    plt.ylabel(r'current $I$')
    plt.show()


def circle(pos):
    (x, y) = pos
    rsq = x ** 2 + y ** 2
    return rsq < 5 ** 2


def make_system():
    gamma = 1
    lat = kwant.lattice.square(norbs=1)
    syst = kwant.Builder()
    syst[lat.shape(circle, (0, 0))] = 4 * gamma
    syst[lat.neighbors()] = -1 * gamma

    def voltage(site, V_static):
        return 2 * gamma + V_static

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
    lead[lat(0, 0)] = voltage
    lead[lat.neighbors()] = -1 * gamma

    leadr = kwant.Builder(kwant.TranslationalSymmetry((0, -1)))
    leadr[lat(0, 0)] = 2 * gamma
    leadr[lat.neighbors()] = -1 * gamma

    syst.attach_lead(lead)
    syst.attach_lead(leadr)
    return lat, syst


def faraday_flux(time, V_dynamic):
    omega = 0.1
    t_upper = pi / omega
    if time <= 0:
        return 0
    if 0 < time < t_upper:
        return V_dynamic * (time - sin(omega * time) / omega) / 2
    return V_dynamic * (time - t_upper) + V_dynamic * t_upper / 2


def tkwant_calculation(syst, operator, tmax, chemical_potential, V_static, V_dynamic):

    params = {'V_static': V_static, 'V_dynamic': V_dynamic}

    # occupation -- for each lead
    occupation_l = tkwant.manybody.lead_occupation(chemical_potential + V_static)
    occupation_r = tkwant.manybody.lead_occupation(chemical_potential)
    occupations = [occupation_l, occupation_r]

    # Create the solver
    # TODO: remove refine=False when doc needs less time to build
    solver = tkwant.manybody.State(syst, tmax, occupations, params=params, refine=False)

    # loop over time, calculating the current as we go
    expectation_values = []
    start = timer.process_time()
    times = range(tmax)
    for time in times:
        solver.evolve(time)
        result = solver.evaluate(operator)
        expectation_values.append(result)
        # if am_master():  # remove comments to have more verbose output
        #    print('[{:.2f}s] t={}, current is {}'
        #          .format(timer.process_time() - start, time, result))

    return times, expectation_values


def main():
    lat, syst = make_system()
    # add the "dynamic" part of the voltage
    tkwant.leads.add_voltage(syst, 0, faraday_flux)
    fsyst = syst.finalized()

    if am_master():
        kwant.plot(syst)

    chemical_potential = 1
    tmax = 200

    # observables
    left_lead_interface = [(lat(0, 0), lat(-1, 0))]
    current_operator = kwant.operator.Current(fsyst, where=left_lead_interface,
                                              sum=True)

    # all the voltage applied via the time-dependenece
    V_static = 0
    V_dynamic = 0.5
    times, tkwant_result = tkwant_calculation(fsyst, current_operator, tmax,
                                              chemical_potential,
                                              V_static, V_dynamic)

    # all the voltage applied statically
    # even though we are doing a "tkwant calculation", we only calculate
    # the results at t=0.
    V_static = 0.5
    V_dynamic = 0
    _, (kwant_result, *_) = tkwant_calculation(fsyst, current_operator, 1,
                                               chemical_potential,
                                               V_static, V_dynamic)

    if am_master():
        plot_currents(times, tkwant_result, kwant_result)


if __name__ == '__main__':
    main()
