:orphan:

.. _restarting:

Restarting calculations from previously saved results
=====================================================

**tkwant features highlighted**

-  restarting calculations from previously saved results

The code can be also found in 
:download:`restarting.py <restarting.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures
    import matplotlib.pyplot as plt
    %matplotlib inline

.. jupyter-execute:: restarting.py

