:orphan:

.. _voltage_raise:

Voltage raise
=============

**Physics Background**

Calculating thermal averages of one-body operators evaluated on
time-evolved single-particle states.

**tkwant features highlighted**

-  Use of ``tkwant.manybody.State``
-  Use of ``tkwant.leads.add_voltage`` to add time-dependence to leads.

The code can be also found in
:download:`voltage_raise.py <voltage_raise.py>`.

.. jupyter-execute::
    :hide-code:
    
    # prevent problems with figures from the first code
    import matplotlib.pyplot as plt
    %matplotlib inline

.. jupyter-execute:: voltage_raise.py

