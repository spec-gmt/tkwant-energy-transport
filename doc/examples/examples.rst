Examples
========

Find below several example problems, highlighting different aspects of tkwant:

:ref:`closed_system`

:ref:`open_system`

:ref:`restarting`

:ref:`alternative_boundary_conditions`

:ref:`voltage_raise`

:ref:`AC_josephson_without_superconductivity`

:ref:`mach_zehnder`.

:ref:`1d_wire`

:ref:`1d_wire_wave_function`

:ref:`1d_wire_onsite`

