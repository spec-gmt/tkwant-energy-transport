from math import sin, pi
import warnings
import time as timer
import matplotlib.pyplot as plt

with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    import kwant
    import tkwant


def am_master():
    """Return true for the MPI master rank"""
    return tkwant.mpi.get_communicator().rank == 0


def plot_currents(times, current):
    plt.plot(times, current, lw=3, label='tkwant')
    plt.legend(loc=4)
    plt.xlabel(r'time $t$')
    plt.ylabel(r'current $I$')
    plt.show()


def barrier(site, time, V_b, V_bias, tau):
    return 2 + V_b


# Raise voltage over a time `tau`
def faraday_flux(time, V_b, V_bias, tau):
    omega = pi / tau
    if time <= 0:
        return 0
    if 0 < time < tau:
        return V_bias * (time - sin(omega * time) / omega) / 2
    return V_bias * (time - tau) + V_bias * tau / 2


def make_system(length):

    lat = kwant.lattice.chain(norbs=1)
    syst = kwant.Builder()
    syst[map(lat, range(length + 2))] = 1
    syst[lat.neighbors()] = -1

    syst[lat(0)] = syst[lat(length)] = barrier

    lead = kwant.Builder(kwant.TranslationalSymmetry((-1,)))
    lead[lat(0)] = 1
    lead[lat.neighbors()] = -1

    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())
    return lat, syst


def main():

    length = 400
    chemical_potential = 1.
    tmax = 500

    V_b = 0.5
    V_bias = 0.2
    tau = 10

    params = {'V_b': V_b, 'V_bias': V_bias, 'tau': tau}

    lat, syst = make_system(length)

    # add the "dynamic" part of the voltage
    tkwant.leads.add_voltage(syst, 0, faraday_flux)
    fsyst = syst.finalized()

    if am_master():
        kwant.plot(syst)

    # Create the solver
    # TODO: remove refine=False when doc needs less time to build
    solver = tkwant.manybody.State(fsyst, tmax, params=params, refine=False)

    # observables
    left_lead_interface = [(lat(length + 1), lat(length))]
    current_operator = kwant.operator.Current(fsyst, where=left_lead_interface,
                                              sum=True)

    # loop over time, calculating the current as we go
    current = []
    start = timer.process_time()
    times = range(tmax)
    for time in times:
        solver.evolve(time)
        result = solver.evaluate(current_operator)
        current.append(result)
        # if am_master():    # remove comments to have more verbose output
        #     print('[{:.2f}s] t={}, current is {}'
        #           .format(timer.process_time() - start, time, r[-1]))

    if am_master():
        plot_currents(times, current)


if __name__ == '__main__':
    main()
