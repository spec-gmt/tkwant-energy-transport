## Important note

This repository is deprecated and is still publicy available to showcase the tests performed to validate the energy transport calculation implementation for the legacy `args` kwant/tkwant approach in [this branch](https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport/-/tree/validation-args/validation-scripts) and the new `params` kwant/tkwant approach in [this branch](https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport/-/tree/validation-params/validation-scripts)

The official energy transport calculations code for tKwant has been implemented as a standalone module called `tkwantoperator`: more information is available in its [code repository](https://gitlab.kwant-project.org/kwant/tkwantoperator)

----------------------------------------

### tkwant-energy-transport

This is a fork of [tkwant](https://gitlab.kwant-project.org/kwant/tkwant) where an additional module, `tkwant.operator`, have been added: it enables the calculation of the time dependant energy related quantities: energy current, source, density and heat current.

This specific branch contains our newer approach in implementing these energy related classes.

### Install instructions
For install instruction and how to run an example script, please refer to <a href="INSTALL.md">`INSTALL.md`</a>.
